package br.jp.sebrae.prova.controller;


import br.jp.sebrae.prova.dto.PostDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/prova")
public class ProvaController {

    @GetMapping("/get/{id}")
    public ResponseEntity<String> recuperar(@PathVariable("id") String id){

        return ResponseEntity.ok(id);
    }

    @PostMapping("/post")
    public ResponseEntity<PostDTO> post(@RequestBody PostDTO dto){

        return ResponseEntity.ok(dto);
    }

}
